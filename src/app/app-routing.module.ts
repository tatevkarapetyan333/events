import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {TableComponent} from './admin/table/table.component';
import {EventsComponent} from './events/events.component';
import {EditComponent} from './admin/edit/edit.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {CreateComponent} from './admin/create/create.component';
import {DeleteComponent} from './admin/delete/delete.component';
import {AuthGuard} from './shared/guards/auth.guard';
import {AdminGuard} from './shared/guards/admin.guard';
import {UserGuard} from './shared/guards/user.guard';
import {LoggedInGuard} from './shared/guards/logged-in.guard';

const routes: Routes = [
    {path: '', redirectTo: 'login', pathMatch: 'full'},
    {path: 'login', component: LoginComponent, canActivate: [LoggedInGuard]},
    {
        path: 'table', component: TableComponent, canActivate: [AuthGuard, AdminGuard], 
        children: [
            {path: 'create', component: CreateComponent},
            {path: 'event/edit/:id', component: EditComponent},
            {path: 'event/:id', component: DeleteComponent}
        ]
    },
    {path: 'grid', component: EventsComponent, canActivate: [AuthGuard, UserGuard]},
    {path: '**', component: PageNotFoundComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
