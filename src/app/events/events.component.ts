import {Component, OnInit} from '@angular/core';
import {EventService} from '../shared/services/event.service';

@Component({
    selector: 'app-events',
    templateUrl: './events.component.html',
    styleUrls: ['./events.component.scss']
})
export class EventsComponent implements OnInit {
    userName = localStorage.getItem('userName');
    allEvents;

    constructor(private eventService: EventService) {
    }

    ngOnInit() {
        this.getAllEvent();
    }

    private getAllEvent() {
        this.eventService.getAllEvents().subscribe(
            res => {
                this.allEvents = res;
            }
        );
    }
}
