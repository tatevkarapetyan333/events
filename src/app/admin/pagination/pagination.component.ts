import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {EventService} from '../../shared/services/event.service';
import {PaginationService} from "../../shared/services/pagination.service";

@Component({
    selector: 'app-pagination',
    templateUrl: './pagination.component.html',
    styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnChanges {
    @Input() totalEventsCount = 0;
    @Input() limit = 0;
    @Output() onPageChange: EventEmitter<number> = new EventEmitter();

    private allEvents;

    constructor(private paginationService: PaginationService) {
    }

    ngOnChanges() {
        
    }

     getEventsTotalCount() {
        return this.paginationService.getEventsTotalCount(1).subscribe(res => {
            this.allEvents = res.body;
        });
    }


}
